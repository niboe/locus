<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="styles.css">
	<link rel="stylesheet" href="calendar.css"> 
</head>
<body>
	<a href="index.php">Main page</a>
	
	<div class="cal">
		<!--
			TODO: Tweak appearance, size, scaling of calendar
			TODO: Once the PHP slugs are finalized, modify the httpd config file to make clean URLs
		-->
		<?php
			include 'Calendar.php';

			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "webdata";
			$conn = new mysqli($servername, $username, $password, $dbname);
			$calendar;
			
			if (array_key_exists('viewdate', $_GET)) {
				$viewdate = new DateTimeImmutable($_GET['viewdate']);
			}
			else {
				$viewdate = new DateTimeImmutable();
			}
			
			if ($conn->connect_error) {
				die($conn->connect_error);
			}

			getmonth($conn, date_format($viewdate, "Y-m-d"));
			
			#TODO: Fix bug when going from the 31st of one month to a month without a 31st day
			echo "<a href='events.php?viewdate=" . date_format($viewdate->sub(date_interval_create_from_date_string("1 month")), "Y-m-d") . "'>Last month</a>\n";
			echo "<a href='events.php?viewdate=" . date_format($viewdate->add(date_interval_create_from_date_string("1 month")), "Y-m-d") . "'>Next month</a>";
			echo $calendar;

			function getmonth($conn, $strdate) {
				global $calendar;
				#TODO bugtest what happens on the first and last day of the month
				#TODO make the end date dynamic as well
				$sql = "SELECT id, Name, Start_Date, End_Date FROM events WHERE Start_Date BETWEEN " . $strdate . " AND '2022-09-03'";
				$calendar = new Calendar($strdate);
				$result = $conn->query($sql);

				if ($result->num_rows > 0) {
					$format = "Y-m-d";
					while ($row = $result->fetch_assoc()) {
						$startdate = new DateTime($row["Start_Date"]);
						$enddate = new DateTime($row["End_Date"]);
						$diff = $startdate->diff($enddate);
						
						$calendar->add_event($row["Name"], $row["Start_Date"], $diff->days + 1, "red", $row["id"]);
					}
				} else {
					echo "no results";
				}
			}
		?>
	</div>
</body>
</html>